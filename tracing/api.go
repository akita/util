package tracing

import (
	"fmt"
	"reflect"

	"gitlab.com/akita/akita/v2/sim"
)

// NamedHookable represent something both have a name and can be hooked
type NamedHookable interface {
	sim.Named
	sim.Hookable
	InvokeHook(sim.HookCtx)
}

// A list of hook poses for the hooks to apply to
var (
	HookPosTaskStart = &sim.HookPos{Name: "HookPosTaskStart"}
	HookPosTaskStep  = &sim.HookPos{Name: "HookPosTaskStep"}
	HookPosTaskEnd   = &sim.HookPos{Name: "HookPosTaskEnd"}
)

// StartTask notifies the hooks that hook to the domain about the start of a
// task.
func StartTask(
	id string,
	parentID string,
	now sim.VTimeInSec,
	domain NamedHookable,
	kind string,
	what string,
	detail interface{},
) {
	task := Task{
		ID:        id,
		ParentID:  parentID,
		StartTime: now,
		Kind:      kind,
		What:      what,
		Where:     domain.Name(),
		Detail:    detail,
	}
	ctx := sim.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskStart,
	}
	domain.InvokeHook(ctx)
}

// StartTaskWithSpecificLocation notifies the hooks that hook to the domain
// about the start of a task, and is able to customize `where` field of a task,
// especially for network tracing.
func StartTaskWithSpecificLocation(
	id string,
	parentID string,
	now sim.VTimeInSec,
	domain NamedHookable,
	kind string,
	what string,
	location string,
	detail interface{},
) {
	task := Task{
		ID:        id,
		ParentID:  parentID,
		StartTime: now,
		Kind:      kind,
		What:      what,
		Where:     location,
		Detail:    detail,
	}
	ctx := sim.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskStart,
	}
	domain.InvokeHook(ctx)
}

// AddTaskStep marks that a milestone has been reached when processing a task.
func AddTaskStep(
	id string,
	now sim.VTimeInSec,
	domain NamedHookable,
	what string,
) {
	step := TaskStep{
		Time: now,
		What: what,
	}
	task := Task{
		ID:    id,
		Steps: []TaskStep{step},
	}
	ctx := sim.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskStep,
	}
	domain.InvokeHook(ctx)
}

// EndTask notifies the hooks about the end of a task.
func EndTask(
	id string,
	now sim.VTimeInSec,
	domain NamedHookable,
) {
	task := Task{
		ID:      id,
		EndTime: now,
	}
	ctx := sim.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskEnd,
	}
	domain.InvokeHook(ctx)
}

// MsgIDAtReceiver generates a standard ID for the message task at the
// message receiver.
func MsgIDAtReceiver(msg sim.Msg, domain NamedHookable) string {
	return fmt.Sprintf("%s@%s", msg.Meta().ID, domain.Name())
}

// TraceReqInitiate generatse a new task. The new task has Type="req_out",
// What=[the type name of the message]. This function is to be called by the
// sender of the message.
func TraceReqInitiate(
	msg sim.Msg,
	now sim.VTimeInSec,
	domain NamedHookable,
	taskParentID string,
) {
	StartTask(
		msg.Meta().ID+"_req_out",
		taskParentID,
		now,
		domain,
		"req_out",
		reflect.TypeOf(msg).String(),
		msg,
	)
}

// TraceReqReceive generates a new task for the message handling. The kind of
// the task is always "req_in".
func TraceReqReceive(
	msg sim.Msg,
	now sim.VTimeInSec,
	domain NamedHookable,
) {
	StartTask(
		MsgIDAtReceiver(msg, domain),
		msg.Meta().ID+"_req_out",
		now,
		domain,
		"req_in",
		reflect.TypeOf(msg).String(),
		msg,
	)
}

// TraceReqComplete terminates the message handling task.
func TraceReqComplete(
	msg sim.Msg,
	now sim.VTimeInSec,
	domain NamedHookable,
) {
	EndTask(MsgIDAtReceiver(msg, domain), now, domain)
}

// TraceReqFinalize terminates the message task. This function should be called
// when the sender receives the response.
func TraceReqFinalize(
	msg sim.Msg,
	now sim.VTimeInSec,
	domain NamedHookable,
) {
	EndTask(msg.Meta().ID+"_req_out", now, domain)
}
